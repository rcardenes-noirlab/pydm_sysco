## Purpose

Sample code showing how to add custom PyDM classes to the Qt Designer. Also, how to add rule-governed properties to new classes (see `pydm_sysco/colorpushbutton.py` for an example)

## Installation

In order to play with the code, simply run `pip install -e .` from the top directory.

Additionally, copy `pydm_sysco_designer_plugin.py` to a path where Designer can find it (eg. the same place where your `pydm_designer_plugin.py` is)
