import json
import sys
from qtpy.QtCore import Slot, Property, Signal
from qtpy.QtCore import QObject, QThread
from qtpy.QtWidgets import QPushButton
from pydm.widgets.base import PyDMWritableWidget
from .redis_interface import prepare_command
import re

ident_re = re.compile(r"[_a-zA-Za]\w*", re.ASCII)

def isident(arg):
    return ident_re.fullmatch(arg) is not None

class SyscoCommandPushButton(QPushButton, PyDMWritableWidget):
    """
    Basic PushButton to trigger a command.

    The SyscoCommandPushButton is meant to hold the specific configuration for a
    command, and send that command to a channel when it is clicked.
    The PyDMPushButton works in two different modes of operation, first, a
    fixed value can be given to the :attr:`.pressValue` attribute, whenever the
    button is clicked a signal containing this value will be sent to the
    connected channel. This is the default behavior of the button. However, if
    the :attr:`.relativeChange` is set to True, the fixed value will be added
    to the current value of the channel. This means that the button will
    increment a channel by a fixed amount with every click, a consistent
    relative move

    Parameters
    ----------
    parent : QObject, optional
        Parent of PyDMPushButton

    label : str, optional
        String to place on button

    icon : QIcon, optional
        An Icon to display on the PyDMPushButton

    endpoint: str
        Command endpoint

    arguments : str
        JSON-encoded list naming the arguments for this command

    argvalues : str
        JSON-encoded dictionary holding values for the arguments

    init_channel : str, optional
        ID of channel to manipulate

    """

    # __Signals__ = ("send_value_signal([dict])")

    # Emitted when the user changes the value.
    send_value_signal = Signal([dict])
    initialized = Signal()

    def __init__(self, parent=None, label=None, icon=None, init_channel=None):
        if icon:
            QPushButton.__init__(self, icon, label, parent)
        elif label:
            QPushButton.__init__(self, label, parent)
        else:
            QPushButton.__init__(self, parent)
        PyDMWritableWidget.__init__(self, init_channel=init_channel)
        self._endpoint = ""
        self._arguments = []
        self._argvalues = {}
        self._sources = ""
        self._timeout = -1
        self.clicked.connect(self.sendValue)
        self.thread = None
        self.initialized.emit()
        self.exec_context = None

    @Property(str)
    def sources(self):
        return self._sources

    @sources.setter
    def sources(self, value):
        if value != self._sources:
            self._sources = value

    @Property(str)
    def endpoint(self):
        """
        This property holds the value to send back through the channel.

        The type of this value does not matter because it is automatically
        converted to match the prexisting value type of the channel. However,
        the sign of the value matters for both the fixed and relative modes.

        Returns
        -------
        str
        """
        return str(self._endpoint)

    @endpoint.setter
    def endpoint(self, value):
        """
        This property holds the value to send back through the channel.

        The type of this value does not matter because it is automatically
        converted to match the prexisting value type of the channel. However,
        the sign of the value matters for both the fixed and relative modes.

        Parameters
        ----------
        value : str
        """
        if str(value) != self._endpoint:
            self._endpoint = str(value)

    @Property(str)
    def arguments(self):
        return ','.join(self._arguments)

    @arguments.setter
    def arguments(self, new_args):
        decoded = [arg.strip() for arg in new_args.split(',')]
        approved = []
        for deco in decoded:
            if not deco.strip():
                continue
            if isident(deco):
                approved.append(deco)
            else:
                print(f"{self.objectName()}: Invalid argument {deco!r}", file=sys.stderr)
        self._arguments = approved

    @Property(str)
    def argvalues(self):
        return json.dumps(self._argvalues)

    @argvalues.setter
    def argvalues(self, new_values):
        try:
            new_values = json.loads(str(new_values))
        except json.decoder.JSONDecodeError:
            print(f"{self.objectName()}: Can't decode {new_values}", file=sys.stderr)
        else:
            if new_values != self._argvalues:
                self._argvalues = new_values

    @Property(float)
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, tout):
        self._timeout = tout

    @Slot(int)
    @Slot(float)
    @Slot(str)
    def updateValue(self, value):
        obj_name = self.sender().objectName() or ""
        left, sep, attribute = obj_name.rpartition("_")
        if attribute not in self._arguments:
            print(f"{self.objectName()}: Received from {self.sender().objectName()} a value for {attribute!r} which is not configured for this control.")
        else:
            self._argvalues[attribute] = value

    def processed_params(self):
        params = {arg:self._argvalues[arg] for arg in self._arguments
                                           if arg in self._argvalues}
        return params

    @Slot()
    def sendValue(self):
        """
        Send a new value to the channel.

        This function interprets the settings of the PyDMPushButton and sends
        the appropriate value out through the :attr:`.send_value_signal`.

        Returns
        -------
        None if any of the following condition is False:
            1. There's no new value (pressValue) for the widget
            2. There's no initial or current value for the widget
            3. The confirmation dialog returns No as the user's answer to the dialog
            4. The password validation dialog returns a validation error
        Otherwise, return the value sent to the channel:
            1. The value sent to the channel is the same as the pressValue if the existing
               channel type is a str, or the relative flag is False
            2. The value sent to the channel is the sum of the existing value and the pressValue
               if the relative flag is True, and the channel type is not a str
        """
        self.setDisabled(True)
        params = self.processed_params()
        print(f"Sending {params} to {self._endpoint}")
        self.exec_context = prepare_command(self, self.endpoint, params, self.timeout)

    @Slot(str)
    def command_error(self, error_message):
        self.setDisabled(False)
        print(f"Error when doing {self._endpoint}...: {error_message}")

    @Slot()
    def command_success(self):
        self.setDisabled(False)
        print("Success for {self._endpoint}!")

    def __execute_send(self, new_value):
        """
        Execute the send operation for push and release.

        Parameters
        ----------
        new_value : int, float or str

        Returns
        -------

        """
        send_value = None
        if new_value is None or self.value is None:
            return None


        if not self._relative or self.channeltype == str:
            send_value = new_value
            self.send_value_signal[self.channeltype].emit(
                self.channeltype(send_value)
            )
        else:
            send_value = self.value + self.channeltype(new_value)
            self.send_value_signal[self.channeltype].emit(send_value)
        return send_value

