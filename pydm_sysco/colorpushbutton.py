from pydm.widgets.pushbutton import PyDMPushButton

_colorProperties = {
        'Color': ['setColor', str]
        }

class PyDMColorPushButton(PyDMPushButton):#, new_properties=_colorProperties):
    """
    Custom PushButton that can react to rules in order to change
    its color.

    Please refer to PyDMPushButton's documentation for the parameters
    accepted by the class.
    """
    def setColor(self, new_value):
        print(f"setColor: {new_value}")
