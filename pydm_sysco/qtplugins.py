from pydm.utilities.iconfont import IconFont
from pydm.widgets.qtplugin_base import (WidgetCategory,
                                        get_widgets_from_entrypoints,
                                        qtplugin_factory)
from pydm.widgets.qtplugin_extensions import (BasicSettingsExtension,
                                              RulesExtension)

from .colorpushbutton import PyDMColorPushButton
from .cmdbutton import SyscoCommandPushButton

ifont = IconFont()

BASE_EXTENSIONS = [BasicSettingsExtension, RulesExtension]

# PushButton that can change colors in response to rule triggers
PyDMColorPushButtonPlugin = qtplugin_factory(PyDMColorPushButton,
                                             group=WidgetCategory.INPUT,
                                             extensions=BASE_EXTENSIONS,
                                             icon=ifont.icon("mouse"))
SyscoCommandPushButtonPlugin = qtplugin_factory(SyscoCommandPushButton,
                                                group=WidgetCategory.INPUT,
                                                extensions=BASE_EXTENSIONS,
                                                icon=ifont.icon("mouse"))

# **********************************************
# NOTE: Add in new PyDM widgets above this line.
# **********************************************

# Add in designer widget plugins from other classes via entrypoints:
globals().update(**get_widgets_from_entrypoints())
