import json
import os
from uuid import uuid4
from time import time

from qtpy.QtCore import QObject, QThread, Signal
from redis import StrictRedis
from sysco.core.conn.rediscon import (
        REDIS_CONNECTION_TIMEOUT,
        REDIS_READ_WRITE_TIMEOUT,
        REDIS_MONITOR_LOOP_DELAY,
        DEFAULT_REDIS_HOST,
        DEFAULT_REDIS_PORT,
        DEFAULT_REDIS_DATABASE
        )

COMMAND_QUEUE = 'command:queue'
CHANNEL_PREFIX = 'command:broadcast'
COMMAND_EXECUTION_WAIT = 1
BROADCAST_LOOP_WAIT = 1

class CommandInterface(QObject):
    error = Signal(str)
    finished = Signal()

    def __init__(self, rd, request, timeout=COMMAND_EXECUTION_WAIT):
        super().__init__()
        self.rd = rd
        self.request = request

    def run(self):
        try:
            encoded = json.dumps(self.request)
            self.rd.rpush(COMMAND_QUEUE, encoded)
        except Exception as e:
            self.error.emit(str(e))
        self.finished.emit()

class SubscribeWorker(QObject):
    finished = Signal()
    success = Signal()
    error = Signal(str)
    started = Signal()

    def __init__(self, rd, uuid, timeout, loop_timeout=BROADCAST_LOOP_WAIT):
        super().__init__()
        self.rd = rd
        self.ps = rd.pubsub()
        self.channel = f'{CHANNEL_PREFIX}:{uuid}'
        self.ps.subscribe(self.channel)
        self.timeout = timeout
        self.loop_timeout = loop_timeout

    def run(self):
        self.started.emit()

        start_time = time()
        try:
            while True:
                if self.timeout > 0:
                    spent = time() - start_time
                    if spent > self.timeout:
                        self.error.emit("Timed out waiting for the command to finish")
                        break
                msg = self.ps.get_message(ignore_subscribe_messages=True, timeout=self.loop_timeout)
                if msg is not None:
                    data = json.loads(msg.get('data', "{}"))
                    st = data.get('status')
                    if st == 'success':
                        self.success.emit()
                        break
                    elif st in {'error', 'critical'}:
                        self.error.emit(data.get('msg', "Error when executing the command"))
                        break
        except Exception as e:
            self.error.emit(f"Exception while processing: {e}")
        finally:
            self.finished.emit()

class Connection:
    rd = None
    @classmethod
    def _initialize(cls):
        host = os.getenv('REDIS_HOST', DEFAULT_REDIS_HOST)
        cls.rd = StrictRedis(host=host,
                             port=DEFAULT_REDIS_PORT,
                             db=DEFAULT_REDIS_DATABASE,
                             socket_connect_timeout=REDIS_CONNECTION_TIMEOUT,
                             socket_timeout=REDIS_READ_WRITE_TIMEOUT,
                             decode_responses=True)

    @classmethod
    def get_server(cls):
        if cls.rd is None:
            cls._initialize()
        return cls.rd

    @classmethod
    def get_command_interface(cls, request):
        return CommandInterface(cls.get_server(), request)

    @classmethod
    def get_subscription_worker(cls, uuid, timeout):
        return SubscribeWorker(cls.get_server(), uuid, timeout)

def prepare_command(parent, endpoint, args, timeout):
    uuid = uuid4().hex
    request = {'version': '1.1',
               'command': endpoint,
               'args': args,
               'uuid': uuid}
    ci = Connection.get_command_interface(request)
    sw = Connection.get_subscription_worker(uuid, timeout)
    t1 = QThread(parent)
    t2 = QThread(parent)

    ci.moveToThread(t1)
    sw.moveToThread(t2)

    t1.started.connect(ci.run)
    t2.started.connect(sw.run)

    ci.finished.connect(t1.quit)
    ci.finished.connect(ci.deleteLater)
    t1.finished.connect(t1.deleteLater)

    sw.started.connect(t1.start)
    sw.success.connect(parent.command_success)
    sw.error.connect(parent.command_error)
    sw.finished.connect(t2.quit)
    sw.finished.connect(sw.deleteLater)
    t2.finished.connect(t2.deleteLater)

    t2.start()

    return t1, t2, ci, sw
